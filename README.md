# python-minimal-password-generator

##### Status: Finished

I wanted to see how small I could make a password generator whilst still including a few basic features. This implementation is in python.

Currently both VIM and Codeberg calculate the python file size at 291 bytes.

The few password personalisation options avaliable take inspiration from acscott's [python cli password generator](https://codeberg.org/acscott-personal/password-generator-cli).
